package com.lia.eopener;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    public String Token;

    //String url = "https://auth.femtos.io";
    //https://femtos.io/login

    //String url = "http://localhost:10000/login";

    String usuario, Password;

    Api_Auth api = new Api_Auth();

    auth_serv auth_serv_data = new auth_serv() ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button boton = (Button) findViewById(R.id.B_Sign_in);
        boton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                usuario = ((TextView) findViewById(R.id.T_Correo)).getText().toString();

                if (!TrueEmail(usuario))
                {
                    ((TextView) findViewById(R.id.T_Correo)).setError("Email no válido");
                    return;
                }

                Password = ((TextView) findViewById(R.id.T_Password)).getText().toString();
                if (IsStringEmpty(Password))
                {
                    ((TextView) findViewById(R.id.T_Password)).setError("Ingrese un valor");
                    return;
                }

                try {
                    Password = computeHash(Password);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void signUp(){

        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Por Favor Espere"); // set message
        progressDialog.show(); // show progress dialog

        api.getClient().registration(usuario.trim(), Password.trim(), new Callback<auth_serv>() {
            @Override
            public void onResponse(Call<auth_serv> success, Response<auth_serv> response) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this,success.toString(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<auth_serv> call, Throwable t) {
              Toast.makeText(MainActivity.this,call.toString(),Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
}


//verificar que la cuenta de correo sea válida
    private boolean TrueEmail (String mail){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(mail).matches();
    }
//Verificar que la contraseña no esté vacía antes de hacer el request
    private boolean IsStringEmpty(String j){
        return j.trim().isEmpty();
    }
//Proceso para generar la encriptación de la contraseña
    private String computeHash(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-256"); digest.reset();
            byte[] byteData = digest.digest(input.getBytes("UTF-8"));
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++)
                {
                    sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
                }
            return sb.toString();
        }
}


