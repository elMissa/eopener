package com.lia.eopener;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Secundario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instruccion);

        Button ingresa = (Button) findViewById(R.id.Btn_Open);

        ingresa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Dispositivo = ((TextView) findViewById(R.id.device_name)).getText().toString();
                TextView resp = (TextView) findViewById(R.id.TXT_Respuesta);

                HttpURLConnection connection = null;

                BufferedReader reader = null;

                String navegador = "ws://torresmac.com:3201/garage_doors/ws?name=";

                navegador.concat(Dispositivo);

                try
                {
                    URL url = new URL(navegador);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();

                    InputStream stream = connection.getInputStream();

                    reader = new BufferedReader(new InputStreamReader(stream));

                    StringBuffer buffer = new StringBuffer();

                    String line = "";
                    while ((line = reader.readLine())  != null){
                        buffer.append(line );
                    }
                resp.setText(buffer.toString());

                } catch (MalformedURLException e){
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                    try {
                        if (reader != null) {
                            reader.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

    }
}
