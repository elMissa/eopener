package com.lia.eopener;

import retrofit2.Retrofit;


public class Api_Auth {

    public static ApiInterface getClient() {

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl("http://localhost:5672") //Set the Root URL
                .build(); //Finally building the adapter

        //Creating object for our interface
        ApiInterface api = adapter.create(ApiInterface.class);
        return api; // return the APIInterface object
    }

}
