package com.lia.eopener;

import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.Callback;
import retrofit2.http.Field;

public interface ApiInterface {

    @FormUrlEncoded // annotation used in POST type requests
    @POST("http://localhost:5672")     // API's endpoints
    public void registration(@Field("username") String name,
                             @Field("password") String password,
                             Callback<auth_serv> callback);

}
